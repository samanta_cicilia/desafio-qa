# language: pt

Funcionalidade: Como usuário quero ter listas de transmissão no WhatsApp
  Criar listas de transmissão com até 256 contatos para transferir mensagens

  Cenario: Criar uma lista de transmissão com dois contatos
    Dado que eu tenha 2 contatos no WhatsApp
    Quando eu clico em "Listas de Transmissão"
    E clico em "Nova Lista"
    E seleciono 2 contatos do WhatsApp
    Então a lista de transmissão é criada com esses 2 contatos

  Cenario: Enviar mensagem através de lista de transmissão
    Dado que já exista uma lista de trasmissão
    Quando eu entro nessa lista
    E envio uma mensagem
    Então os contatos dessa lista recebem uma mensagem a partir do meu usuário

  Cenário: Não criar listas de transmissão com apenas 1 contato
    Dado que eu tenha 1 contato no WhatsApp
    Quando eu clico em "Listas de Transmissão"
    E clico em "Nova Lista"
    E seleciono esse contato
    Então não é habilitado o botão de Criar lista
