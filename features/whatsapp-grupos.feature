# language: pt

Funcionalidade: Como usuário quero ter grupos no WhatsApp
  Criar grupos com até 256 contatos para transferir mensagens

  Cenario: Criar um grupo com dois contatos
    Dado que eu tenha 2 contatos no WhatsApp
    Quando eu clico em "Novo Grupo"
    E dou um nome para esse grupo
    E seleciono 2 contatos do WhatsApp
    Então o grupo é criado

  Cenario: Incluir um contato em um grupo já existente
    Dado que já exista o grupo "Amigos"
    Quando eu entro na área de detalhes desse grupo
    E clico em cima do participante "Jose"
    E clico em "Remover Jose"
    E confirmo a remoção através do "Remover"
    Então "Jose" não faz mais parte do grupo "Amigos"


