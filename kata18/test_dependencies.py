# coding=utf-8

import unittest

from kata18.dependencies import Dependencies

__author__ = 'samanta'


class TestDependencies(unittest.TestCase):
    def setUp(self):
        self.dependencies = Dependencies()

    def tearDown(self):
        self.dependencies = None

    def test_add_direct(self):
        expected_result = {'A': ['B', 'C', 'D']}
        actual_result = self.dependencies.add_direct('A', ['B', 'C', 'D'])
        self.assertEqual(expected_result, actual_result)

    def test_basic(self):
        self.dependencies.add_direct('A', ['B', 'C'])
        self.dependencies.add_direct('B', ['C', 'E'])
        self.dependencies.add_direct('C', ['G'])
        self.dependencies.add_direct('D', ['A', 'F'])
        self.dependencies.add_direct('E', ['F'])
        self.dependencies.add_direct('F', ['H'])

        self.assertEqual(['B', 'C', 'E', 'F', 'G', 'H'], self.dependencies.dependencies_for('A'))
        self.assertEqual(['C', 'E', 'F', 'G', 'H'], self.dependencies.dependencies_for('B'))
        self.assertEqual(['G'], self.dependencies.dependencies_for('C'))
        self.assertEqual(['A', 'B', 'C', 'E', 'F', 'G', 'H'], self.dependencies.dependencies_for('D'))
        self.assertEqual(['F', 'H'], self.dependencies.dependencies_for('E'))
        self.assertEqual(['H'], self.dependencies.dependencies_for('F'))

    def test_recursivity(self):
        self.dependencies.add_direct('A', ['B'])
        self.dependencies.add_direct('B', ['C'])
        self.dependencies.add_direct('C', ['A'])

        self.assertEqual(['B', 'C'], self.dependencies.dependencies_for('A'))
        self.assertEqual(['A', 'C'], self.dependencies.dependencies_for('B'))
        self.assertEqual(['A', 'B'], self.dependencies.dependencies_for('C'))

    def test_recursivity_two(self):
        self.dependencies.add_direct('A', ['B'])
        self.dependencies.add_direct('B', ['A'])
        self.dependencies.add_direct('C', ['B'])

        self.assertEqual(['B'], self.dependencies.dependencies_for('A'))
        self.assertEqual(['A'], self.dependencies.dependencies_for('B'))
        self.assertEqual(['A', 'B'], self.dependencies.dependencies_for('C'))


if __name__ == '__main__':
    unittest.main()
