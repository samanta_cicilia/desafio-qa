# coding=utf-8

__author__ = 'samanta'


class Dependencies(object):
    def __init__(self):
        self.itens = {}
        self.dependencies_item = []
        self.item = None

    def add_direct(self, item, dependencies):
        self.itens.update({item: dependencies})
        return self.itens

    def dependencies_for(self, item):
        self.item = item
        self.get_dependencies_by_name(item)
        if item in self.dependencies_item:
            self.dependencies_item.remove(item)
        dependencies_ordered = sorted(self.dependencies_item)
        self.dependencies_item = []
        return dependencies_ordered

    def get_dependencies_by_name(self, item):
        if item in self.itens:
            dependencies = self.itens[item]
            for dependencie in dependencies:
                if dependencie in self.dependencies_item:
                    return
                else:
                    self.add_dependencies(dependencie)
                    self.get_dependencies_by_name(dependencie)
        else:
            self.add_dependencies(item)

    def add_dependencies(self, item):
        if not item in self.dependencies_item:
            self.dependencies_item.append(item)


if __name__ == '__main__':
    dep = Dependencies()
    list_of_keys = []
    qt = input("Would you like enter with how many keys and dependencies? (Ex: 2)")
    i = 1
    while i <= int(qt):
        key = input("Enter with key: ")
        dependencies = input("Enter with dependencies separated by space. (Ex: B C) ")
        dependencies_list = [str(x) for x in dependencies.split()]
        dep.add_direct(key, dependencies_list)
        i += 1
        list_of_keys.append(key)

    for key in list_of_keys:
        print (key)
        print (dep.dependencies_for(key))
