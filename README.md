# How to run this project

1 - Create a virtualenv with Python 3.5.1 

2 - Go to folder 'desafio-qa'

3 - Run pip install -r requirements.txt

4 - To run main class use python dependencies.py

5 - To run tests use nosetests --with-coverage
